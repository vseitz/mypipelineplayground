# myapp/app.py
import argparse
import json
import os
import json
from datetime import datetime
# SQLite for data storage
import sqlite3
from enum import Enum


class DatabaseMode(Enum):
    APPENDING=1
    CREATING=2

TALP_TABLE_NAME="talp_data"
TALP_TABLE_COLUMNS_WITH_DATATYPES="(timestamp TIMESTAMP, talp_ouput TEXT, metadata TEXT)"
TALP_TABLE_COLUMNS="(timestamp ,talp_ouput, metadata)"


# Function to insert data into the SQLite database
def insert_data(conn,timestamp, talp_output, metadata):
    # Connect to the SQLite database
    cursor = conn.cursor()

    try:
        # Create a table if it doesn't exist
        cursor.execute(f"CREATE TABLE IF NOT EXISTS {TALP_TABLE_NAME} {TALP_TABLE_COLUMNS_WITH_DATATYPES}")
        
        # Create an index on the timestamp column
        cursor.execute(f"CREATE INDEX IF NOT EXISTS idx_timestamp ON {TALP_TABLE_NAME} (timestamp)")
        
        # Convert JSON objects to string format

        # Insert data into the table
        cursor.execute(f"INSERT INTO {TALP_TABLE_NAME} {TALP_TABLE_COLUMNS} VALUES (?, ?, ?)", (timestamp, json.dumps(talp_output), json.dumps(metadata)))

        # Commit changes and close the connection
        conn.commit()
        print("INFO: Data inserted successfully")
    except sqlite3.Error as e:
        print("ERROR:", e)
    finally:
        # Close the connection
        conn.close()

def main():
    # Parse command-line arguments
    parser = argparse.ArgumentParser(description='Add talp.json to the local time series database')
    parser.add_argument('-t','--talp',dest='talp', help='talp.json file to be added',required=True)
    parser.add_argument('-m','--metadata',dest='metadata', help='metadata.json file to be added',required=False)
    parser.add_argument('-db','--database',dest='database', help='TALP.db file. If not specified a new one will be generated', required=False)
    # TODO add timestamp mechanism
    args = parser.parse_args()

    # Check if the JSON file exists
    if not os.path.exists(args.talp):
        print(f"Error: The specified JSON file '{args.talp}' does not exist.")
        return

    if args.metadata:
        if not os.path.exists(args.metadata):
            print(f"Error: The specified JSON file '{args.metadata}' does not exist.")
            return

    # Set output 
    if args.database:
        DB_FILE=args.database
        if os.path.exists(DB_FILE):
            DB_MODE=DatabaseMode.APPENDING
        else:
            DB_MODE=DatabaseMode.CREATING
        
    else:
        DB_FILE="TALP.db"
        DB_MODE=DatabaseMode.CREATING

    # Connect to database
    conn = sqlite3.connect(DB_FILE)

    
    current_timestamp=datetime.now()

    with open(args.talp, 'r') as json_file:
        try:
            talp_output = json.load(json_file)
        except json.JSONDecodeError as e:
            print(f"Error decoding JSON: {e}")
            return
    if args.metadata:
        with open(args.metadata, 'r') as json_file:
            try:
                metadata = json.load(json_file)
            except json.JSONDecodeError as e:
                print(f"Error decoding JSON: {e}")
                return
    else:
        metadata={}
        
    insert_data(conn,current_timestamp,talp_output,metadata)



if __name__ == "__main__":
    main()
