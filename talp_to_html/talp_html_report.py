# myapp/app.py
import argparse
import json
import os
from jinja2 import Environment, FileSystemLoader
from urllib.request import urlopen, Request
import pathlib
TALP_TEMPLATE_PATH = pathlib.Path(__file__).parent.resolve()


def render_template(directory, template_name, **context):
    # Set up Jinja2 environment and load the template
    env = Environment(loader=FileSystemLoader(directory))
    template = env.get_template(template_name)
    
    # Render the template with the provided context
    return template.render(context)


def create_badge(parallel_eff):
    if parallel_eff < 0.6:
        bagde_url=f"https://img.shields.io/badge/Parallel_efficiency-{parallel_eff}-red"
    elif parallel_eff < 0.8:
        bagde_url=f"https://img.shields.io/badge/Parallel_efficiency-{parallel_eff}-orange"
    else:
        bagde_url=f"https://img.shields.io/badge/Parallel_efficiency-{parallel_eff}-green"
    
    return urlopen(Request(url=bagde_url, headers={'User-Agent': 'Mozilla'})).read()

def main():
    # Parse command-line arguments
    parser = argparse.ArgumentParser(description='Render HTML summary of the talp.json')
    parser.add_argument('input', help='Path to the TALP JSON file')
    parser.add_argument('-o','--output',dest='output', help='Name of the html file beeing generated. If not specified [input].html will be chosen', required=False)
    parser.add_argument('-b','--badge', dest='badge',default="parallel_efficiency.svg",help="Create a [badge].svg using shields.io. Internet access needed")
    args = parser.parse_args()

    # Check if the JSON file exists
    if not os.path.exists(args.input):
        print(f"Error: The specified JSON file '{args.json_file}' does not exist.")
        return

    # Set output 
    if args.output:
        OUTPUT_FILE=args.output
        if not args.output.endswith('.html'):
            OUTPUT_FILE += ".html"
            print(f"Info: Appending .html to '{args.output}'")
        # Check if the HTML file exists
        if os.path.exists(args.output):
            print(f"Info: Overwriting '{args.output}'")
    else:
        OUTPUT_FILE=args.input.replace(".json","") 
        OUTPUT_FILE+= ".html"

    # Load data from the JSON file
    with open(args.input, 'r') as json_file:
        try:
            raw_data = json.load(json_file)
        except json.JSONDecodeError as e:
            print(f"Error decoding JSON: {e}")
            return

    # Check if the popMetrics are there: 
    if not raw_data['popMetrics']:
        print(f"No popMetrics found in TALP json. Try re-running DLB with arguments --talp --talp-summary=pop-metrics --talp-file={args.input}")
        return
    
    pop_metric_regions=raw_data['popMetrics']
    # Render the template with the data
    rendered_html = render_template(TALP_TEMPLATE_PATH,'talp_report.jinja',regions=pop_metric_regions)

    # Save or display the rendered HTML as needed
    with open(OUTPUT_FILE, 'w') as f:
        f.write(rendered_html)

    
    
    if args.badge:
        if not args.badge.endswith(".svg"):
            args.badge += ".svg"
        # Get the global effiency
        parallel_efficiency=0.0
        for region in pop_metric_regions:
            if region['name'] == "MPI Execution":
                parallel_efficiency = region['parallelEfficiency']
        
        badge = create_badge(parallel_efficiency)
        with open(args.badge, 'wb') as f:
            f.write(badge)


if __name__ == "__main__":
    main()
